<?php

namespace app\controllers;

use Yii;
use app\models\AuthorsRecord;
use app\models\AuthorsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
/**
 * AuthorsController implements the CRUD actions for AuthorsRecord model.
 */
class AuthorsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthorsRecord models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuthorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthorsRecord model.
     * @param integer $id
     * @param string $name
     * @param string $surname
     * @return mixed
     */
    public function actionView($id, $name, $surname)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $name, $surname),
        ]);
    }

    /**
     * Creates a new AuthorsRecord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
        $model = new AuthorsRecord();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->goBack();
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function storeReturnUrl() {
        Yii::$app->user->returnUrl = Yii::$app->request->url;
    }
    
    public function actionBooks($id) {
        $model  = AuthorsRecord::findOne($id);
        $model = $model->idbooks;
        $dataProvider = new \yii\data\ArrayDataProvider(
          [ 'allModels' => $model,
            'pagination' => false
          ]
        );

       return $this->render('authorbooks', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    
    /**
     * Updates an existing AuthorsRecord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $name
     * @param string $surname
     * @return mixed
     */
    public function actionUpdate($id, $name, $surname)
    {
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
        $model = $this->findModel($id, $name, $surname);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'name' => $model->name, 'surname' => $model->surname]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthorsRecord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $name
     * @param string $surname
     * @return mixed
     */
    public function actionDelete($id, $name, $surname)
    {
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
        $this->findModel($id, $name, $surname)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthorsRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $name
     * @param string $surname
     * @return AuthorsRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $name, $surname)
    {
        if (($model = AuthorsRecord::findOne(['id' => $id, 'name' => $name, 'surname' => $surname])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена');
        }
    }
}
