<?php

namespace app\controllers;

use Yii;
use app\models\RubricsRecord;
use app\models\RubricsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
/**
 * RubricsController implements the CRUD actions for RubricsRecord model.
 */
class RubricsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RubricsRecord models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RubricsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RubricsRecord model.
     * @param integer $id
     * @param string $name
     * @return mixed
     */
    public function actionView($id, $name)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $name),
        ]);
    }

    /**
     * Creates a new RubricsRecord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
        $model = new RubricsRecord();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->goBack();
      
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    
     public function actionBooks($id) {
        $model  = RubricsRecord::findOne($id);
        $model = $model->idbooks;
        $dataProvider = new \yii\data\ArrayDataProvider(
          [ 'allModels' => $model,
            'pagination' => false
          ]
        );

       return $this->render('rubricbooks', [
            'dataProvider' => $dataProvider
        ]);
        }
        
        
    /**
     * Updates an existing RubricsRecord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $name
     * @return mixed
     */
    public function actionUpdate($id, $name)
    {
     
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
        $model = $this->findModel($id, $name);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'name' => $model->name]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RubricsRecord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $name
     * @return mixed
     */
    public function actionDelete($id, $name)
    {
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
        $this->findModel($id, $name)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RubricsRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $name
     * @return RubricsRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $name)
    {
        if (($model = RubricsRecord::findOne(['id' => $id, 'name' => $name])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Данная страница не найдена');
            return $model;
        }
    }
}
