<?php

namespace app\controllers;

use Yii;

use app\models\BooksRecord;
use app\models\AuthorsRecord;
use app\models\BookRubricRecord;
use app\models\BookAuthorRecord;
use app\models\BooksSearch;
use app\models\Author;
use app\models\Rubric;

use yii\web\ForbiddenHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BooksController implements the CRUD actions for BooksRecord model.
 */
class BooksController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BooksRecord models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BooksRecord model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BooksRecord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
        
        $this->storeReturnUrl();
     
        $model = new BooksRecord;
        $author = new Author;
        $rubric = new Rubric;
        
        if (Yii::$app->request->isPost && 
            $model->tryLoad(Yii::$app->request->post(), UploadedFile::getInstance($model, 'foto')) && 
            $author->load(Yii::$app->request->post()) && 
            $rubric->load(Yii::$app->request->post()) && 
            $model->save()) {
              foreach($author->idauthors as $idauthor) {
                    $bookauth = new BookAuthorRecord();
                    $bookauth->idbook = $model->id;
                    $bookauth->idauthor = $idauthor;
                    $bookauth->save(); 
                }
                
              foreach($rubric->idrubrics as $idrubric) {
                    $bookrubr = new BookRubricRecord();
                    $bookrubr->idbook = $model->id;
                    $bookrubr->idrubric = $idrubric;
                    $bookrubr->save(); 
                }
                
            return $this->redirect(['view', 'id' => $model->id]);
         
         }
         else {
            return $this->render('create', compact('model', 'author', 'rubric'));//['model' => $model]);
        }
    }
    
    public function storeReturnUrl() {
        Yii::$app->user->returnUrl = Yii::$app->request->url;
    }
    
    /**
     * Updates an existing BooksRecord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
     
        $this->storeReturnUrl();
        $model = $this->findModel($id);
        $author = new Author;
        $rubric = new Rubric;
        
        /*if ($model->load(Yii::$app->request->post()) && $model->upload(UploadedFile::getInstance($model, 'foto')) && $model->save()) {*/
        if(Yii::$app->request->isPost && 
            $model->tryLoad(Yii::$app->request->post(), UploadedFile::getInstance($model, 'foto')) && 
            $author->load(Yii::$app->request->post()) && 
            $rubric->load(Yii::$app->request->post()) && 
            $model->save()) {
                
            BookAuthorRecord::deleteAll("idbook = {$model->id}");
              foreach($author->idauthors as $idauthor) {
                    $bookauth = new BookAuthorRecord();
                    $bookauth->idbook = $model->id;
                    $bookauth->idauthor = $idauthor;
                    $bookauth->save();
                }
                
            BookRubricRecord::deleteAll("idbook = {$model->id}");
              foreach($rubric->idrubrics as $idrubric) {
                    $bookrubr = new BookRubricRecord();
                    $bookrubr->idbook = $model->id;
                    $bookrubr->idrubric = $idrubric;
                    $bookrubr->save(); 
                }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', compact('model', 'author', 'rubric'));//['model' => $model]);
            /*return $this->render('update', [
                'model' => $model,
            ]);*/
        }
    }

    /**
     * Deletes an existing BooksRecord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
     
        if(Yii::$app->user->isGuest) throw new ForbiddenHttpException('Вы не зарегистрированный пользователь. Пожалуйста, авторизируйтесь.');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BooksRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BooksRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BooksRecord::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
