<?php
namespace app\assets;

use yii\web\AssetBundle;

class ApplicationAsset extends AssetBundle {
	public $sourcePath = '@app/assets/ui';
	public $css = ['css/main.css'];
	public $js = ['js/main.js'];
	public $depends = [
		'yii\bootstrap\bootstrapAsset',
		'yii\web\YiiAsset'
	];
} 
?>