<?php

namespace app\models;
use Yii;

/**
 * This is the model class for table "rubrics".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Bookrubrics[] $bookrubrics
 * @property Books[] $idbooks
 */
class RubricsRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubrics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Рубрика',
        ];
    }
    
    
    public function getNames() {
       return $this->find()->indexBy('id')->all();
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookrubrics()
    {
        return $this->hasMany(Bookrubrics::className(), ['idrubric' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdbooks()
    {
        return $this->hasMany(BooksRecord::className(), ['id' => 'idbook'])->viaTable('bookrubrics', ['idrubric' => 'id']);
    }
}
