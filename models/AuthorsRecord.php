<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 *
 * @property Bookauthor[] $bookauthors
 * @property Books[] $idbooks
 */
class AuthorsRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $fullname;
 
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname'], 'required'],
            [['name', 'surname'], 'string', 'max' => 50]
        ];
    }

    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'name' => 'Имя',
            'surname' => 'Фамилия',
        ];
    }

    
    public function getAllAuthors() {
      return $this->find()->indexBy('id')->all();
    }
    
    public function getAllFullNames () {
      return $this->findBySql('SELECT id, CONCAT(surname, " ", name) as `fullname` FROM authors')->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookauthors()
    {
        return $this->hasMany(Bookauthor::className(), ['idauthor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdbooks()
    {
        return $this->hasMany(BooksRecord::className(), ['id' => 'idbook'])->viaTable('bookauthor', ['idauthor' => 'id']);
    }
}
