<?php
namespace app\models;

use Yii;

class Rubric extends \yii\base\Model {

	public $idrubrics;
	
	public function __construct() {
		$idrubrics = [];
	}
	
	public function formName() {
		return 'Rubric';
	}
	
	public function rules() {
		return [
				[['idrubrics'],'integer'],
				[['idrubrics'],'required'],
		];
	}
	
	public function attributeLabels() {
		return [
			'idrubrics' => 'Рубрики:',
		];
	}
}

?>