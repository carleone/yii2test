<?php
namespace app\models;

use Yii;

class Author extends \yii\base\Model {

	public $idauthors;
	
	public function __construct() {
		$idauthors = [];
	}
	
	public function formName() {
		return 'Author';
	}
	
	public function rules() {
		return [
				[['idauthors'],'integer'],
				[['idauthors'],'required'],
		];
	}
	
	public function attributeLabels() {
		return [
			'idauthors' => 'Авторы:',
		];
	}
}

?>