<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property string $foto
 *
 * @property Bookauthor[] $bookauthors
 * @property Authors[] $idauthors
 * @property Bookrubrics[] $bookrubrics
 * @property Rubrics[] $idrubrics
 */
class BooksRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
   
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            /*[['foto'], 'file', 
                       'skipOnEmpty' => false,
                       'extensions' => 'png, jpg, gif, jpeg',
                       'maxSize' => 1024*1024*5,
            ]*/
            [['foto'], 'required','message' => 'Выберите, пожалуйста, файл обложки для книги'],
            [['foto'], 'file','extensions' => 'png, jpg, gif, jpeg',
                       'maxSize' => 1024*1024*5,
                       'message' => 'Не правильный тип файла или размер > 5Мб'
            ],
            [['name'],'string', 'message' => 'Должно быть введены только печатные символы'],
            
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название книги',
            'foto' => 'Фотография книги',
            'idauthors' => 'Доступные авторы'
        ];
    }
    
    
    
    public function upload($fileObj) {
        if($fileObj->error === 0) {
            $file = fopen($fileObj->tempName, "r");
            $this->foto = "data:{$fileObj->type};base64,".base64_encode(fread($file, $fileObj->size));
            @fclose($file);
             return true; 
         } else {
           return false;
         }
    }
   
   

    public function tryLoad($post, $fileObj) {
       if($fileObj === false) {
         return parent::load($post);
        }
       else {
          if($fileObj->error === 0) {
            $file = fopen($fileObj->tempName, "r");
            $post[$this->formName()]['foto'] = "data:{$fileObj->type};base64,".base64_encode(fread($file, $fileObj->size));
            @fclose($file);
          } 
         return parent::load($post);
        }
     }
     
    public function setBookauthors() {
         $bookauth = new BookAuthor();
         $bookauth->idbook = $this->id;
         $bookauth->idauthor = $this->idauthors;
         return $bookauth->save();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookauthors()
    {
        return $this->hasMany(Bookauthor::className(), ['idbook' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdauthors()
    {
        return $this->hasMany(AuthorsRecord::className(), ['id' => 'idauthor'])->viaTable('bookauthor', ['idbook' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookrubrics()
    {
        return $this->hasMany(Bookrubrics::className(), ['idbook' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdrubrics()
    {
        return $this->hasMany(RubricsRecord::className(), ['id' => 'idrubric'])->viaTable('bookrubrics', ['idbook' => 'id']);
    }
}
