<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use app\models\BooksRecord;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Записи по книгам';
$this->params['breadcrumbs'][] = $this->title;
app\assets\ApplicationAsset::register($this);
?>
<div class="books-record-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
         <?php if(!Yii::$app->user->isGuest): ?>
        <?= Html::a('Добавить новую книгу', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'foto:image',
            [
                'label' => 'Автор(ы)',
                'format' => 'paragraphs',
                'value' => function($model) {
                    $return = $model->idauthors;
                    $result = '';   
                    foreach($return as $author) {
                        $result .= implode(' ',array_filter($author->getAttributes(['name','surname']))). "\n\n";
                    }
                return $result;
                }
            ],
            [
                'label' => 'Рубрика(и)',
                'format' => 'paragraphs',
                'value' => function($model) {
                    $return = $model->idrubrics;
                    $result = '';
                    foreach($return as $rubric) {
                        $result .= implode(' ',$rubric->getAttributes(['name']))."\n\n";
                    }
                return $result;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visible' => !Yii::$app->user->isGuest
            ],
        ],
    ]); ?>

</div>
