<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BooksRecord */

$this->title = 'Добавить книгу';
$this->params['breadcrumbs'][] = ['label' => 'Запись о книгах', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-record-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'author' => $author,
        'rubric' => $rubric
    ]) ?>
</div>
