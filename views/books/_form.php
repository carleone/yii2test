<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use app\models\AuthorsRecord;
use app\models\RubricsRecord;
use app\models\Author;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\BooksRecord */
/* @var $form yii\widgets\ActiveForm */


?>
<div class="books-record-form">

    <?php $form = ActiveForm::begin([
                                'id' => 'create-book-form',
                                'options' => ['enctype' => 'multipart/form-data'],
                                ]);
        
        $authors = new AuthorsRecord;
        $rubrics = new RubricsRecord;
    ?>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'foto')->fileInput(['accept' => 'image/*']) ?>
    
    <?= $form->field($author, 'idauthors')->listBox(ArrayHelper::map($authors->allFullNames, 'id', 'fullname'), ['multiple' => true]) ?>
    <?= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp&nbspДобавить автора.',['authors/create'])?>
    
    <?= $form->field($rubric, 'idrubrics')->listBox(ArrayHelper::map($rubrics->names, 'id', 'name'), ['multiple' => true]) ?>
    <?= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp&nbspДобавить рубрику.',['rubrics/create'])?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
