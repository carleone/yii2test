<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
    // echo $this->render('_search', ['model' => $searchModel]); 

$this->title = 'Список книг';
$this->params['breadcrumbs'][] = $this->title;
app\assets\ApplicationAsset::register($this);

?>
<div class="authors-record-index">

    <h1><?= Html::encode($this->title) ?></h1>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'foto:image',
            ],
    ]); ?>
</div>
