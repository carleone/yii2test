<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RubricsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рубрики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubrics-record-index">

    <h1><?= Html::encode($this->title) ?></h1>
  
    <p> <?php if(!Yii::$app->user->isGuest): ?>
        <?= Html::a('Добавить рубрику', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Рубрика',
                'format' =>'html',
                'value' => function($model) {
                    return sprintf("%s",Html::a($model->name, 'books/?id='.$model->id));
                    }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visible' => !Yii::$app->user->isGuest
            ],
        ],
    ]); ?>

</div>
