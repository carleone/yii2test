<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AuthorsRecord */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Авторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="authors-record-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p> <?php if(!Yii::$app->user->isGuest):?>
        <?= Html::a('Обновить', ['update', 'id' => $model->id, 'name' => $model->name, 'surname' => $model->surname], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id, 'name' => $model->name, 'surname' => $model->surname], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
        <?php endif?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'surname',
        ],
    ]) ?>

</div>
