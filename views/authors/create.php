<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AuthorsRecord */

$this->title = 'Добавление автора.';
$this->params['breadcrumbs'][] = ['label' => 'Записи авторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="authors-record-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
