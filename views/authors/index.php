<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список авторов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="authors-record-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p> <?php if(!Yii::$app->user->isGuest): ?>
        <?= Html::a('Добавить нового автора', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'label' => '№',
                'format'=>'html',
                'value'=> function($model) {
                    return sprintf("%s",Html::a($model->id, 'books/?id='.$model->id));
                    }
            ],
            'name',
            'surname',
            [   
                'class' => 'yii\grid\ActionColumn',
                'visible' => !Yii::$app->user->isGuest
            ],
        ],
    ]); ?>

</div>
