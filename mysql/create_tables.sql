-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema netpeak
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema netpeak
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `netpeak` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `netpeak` ;

-- -----------------------------------------------------
-- Table `netpeak`.`books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netpeak`.`books` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `foto` MEDIUMBLOB NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `netpeak`.`authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netpeak`.`authors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `surname` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`, `name`, `surname`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `netpeak`.`rubrics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netpeak`.`rubrics` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`, `name`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `netpeak`.`bookrubrics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netpeak`.`bookrubrics` (
  `idrubric` INT NOT NULL,
  `idbook` INT NOT NULL,
  PRIMARY KEY (`idrubric`, `idbook`),
  INDEX `fk_bookrubrics_books_idx` (`idbook` ASC),
  CONSTRAINT `fk_bookrubrics_books`
    FOREIGN KEY (`idbook`)
    REFERENCES `netpeak`.`books` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bookrubrics_rubrics`
    FOREIGN KEY (`idrubric`)
    REFERENCES `netpeak`.`rubrics` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `netpeak`.`bookauthor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netpeak`.`bookauthor` (
  `idbook` INT NOT NULL,
  `idauthor` INT NOT NULL,
  PRIMARY KEY (`idbook`, `idauthor`),
  INDEX `fk_bookauthor_authors1_idx` (`idauthor` ASC),
  CONSTRAINT `fk_bookauth_book`
    FOREIGN KEY (`idbook`)
    REFERENCES `netpeak`.`books` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bookauth_auth`
    FOREIGN KEY (`idauthor`)
    REFERENCES `netpeak`.`authors` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
